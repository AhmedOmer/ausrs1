﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;

namespace AUSRS.Data.Repositories
{
    public class UnitOfWork : IUintOfWork
    {
        private readonly AdenUniversityContext _context;
        private Hashtable _repositories;

        public UnitOfWork(AdenUniversityContext db)
        {
            _context = db;
        }
        public UnitOfWork()
        {
            _context = new AdenUniversityContext();
        }

        
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public IRepository<T> GetRepositry<T>() where T : class
        {
            if (_repositories == null)
                _repositories = new Hashtable();

            var type = typeof(T).Name;

            if (!_repositories.ContainsKey(type))
            {
                var repositoryType = typeof(Repository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), _context);
                _repositories.Add(type, repositoryInstance);
            }

            return (IRepository<T>)_repositories[type];
        }

        public IEnumerable<DbValidationError> ValidationErrors
        {
            get
            {
                List<DbValidationError> errors = new List<DbValidationError>();
                foreach (var dbErrors in _context.GetValidationErrors())
                {
                    foreach (var item in dbErrors.ValidationErrors)
                    {
                        errors.Add(new DbValidationError(item.PropertyName,item.ErrorMessage));
                    }
                }
                return errors;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
