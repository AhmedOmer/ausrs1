﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;

namespace AUSRS.Data.Repositories
{
    public interface IUintOfWork : IDisposable
    {
        int SaveChanges();

        IEnumerable<DbValidationError> ValidationErrors { get; }

        IRepository<T> GetRepositry<T>() where T : class;
    }
}
