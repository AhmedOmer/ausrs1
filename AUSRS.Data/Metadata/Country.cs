﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AUSRS.Data
{
    [MetadataType(typeof(Metadata))]
    public partial class Country
    {
        class Metadata
        {
            [Display(Name = "الاسم")]
            [Required(ErrorMessage ="{0} - حقل مطلوب")]
            public string Name { get; set; }
            public string NameEn { get; set; }
            public string Nationality { get; set; }
            public string NationalityEn { get; set; }
        }
    }
}
