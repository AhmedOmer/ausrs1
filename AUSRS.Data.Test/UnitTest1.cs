﻿using System;
using System.Linq;
using AUSRS.Data.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AUSRS.Data.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var country = new Country();
            using(IUintOfWork db = new UnitOfWork())
            {
                var repository = db.GetRepositry<Country>();
                repository.Add(country);

                int errors = db.ValidationErrors.Count();
                Assert.AreEqual(2, errors);
            }
        }
    }
}
