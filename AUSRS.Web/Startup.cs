﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AUSRS.Web.Startup))]
namespace AUSRS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
